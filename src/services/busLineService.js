import axios from 'axios';

const baseUrl = 'https://data.itsfactory.fi/journeys/api/1/lines';

export const getBusLines = async() => {
  const result = await axios.get(baseUrl);
  return result.data;
}