import {
  Routes,
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

import Home from './pages/Home';
import Bingo from './pages/Bingo.js';
import BusLine from './pages/BusLine.js';
import Todos from './pages/Todos.js';
import Cards from './pages/Cards.js';
import Clock from './pages/Clock.js';

const App = () => {
  return (
    <div className = "App" >
      <Router>
        <nav>
          <Link className='menu-link' to='/'>Home</Link>
          <Link className='menu-link' to='/bingo'>Bingo!</Link>
          <Link className='menu-link' to='/buslines'>Tampere bus lines</Link>
          <Link className='menu-link' to='/todos'>Todo-app</Link>
          <Link className='menu-link' to='/cards'>Color memory game</Link>
          <Link className='menu-link' to='/clock'>Clock</Link>
        </nav> 
        <Routes>
          <Route path='/' element={<Home/>}></Route>
          <Route path='/bingo' element={<Bingo/>}></Route>
          <Route path='/buslines' element={<BusLine/>}></Route>
          <Route path='/todos' element={<Todos/>}></Route>
          <Route path='/cards' element={<Cards size='8'/>}></Route>
          <Route path='/clock' element={<Clock/>}></Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
