import {useState, useEffect} from 'react';
import {getBusLines} from '../services/busLineService.js';
import '../styles/busline.css';

const BusLines = () => {
  const [busLines, setBusLines] = useState([]);
  useEffect(() => {
    const getBuslines = async() => {
      const buslines = await getBusLines()
      setBusLines(buslines.body);
    }
    getBuslines();
  }, [])

  return (
    <table>
      <tbody>
        <tr key='headerRow'>
          <th key='numberColumnA'>Numero</th>
          <th key='numberColumnB'>Linja</th>
        </tr>
        {busLines.map((line, idx) => {
          return (
            <tr key={'row'+idx}>
              <td key={idx + 'A'}>{line.name}</td>
              <td key={idx + 'B'}>{line.description}</td>
            </tr>
          )
        })}  
      </tbody>
    </table>
  )

}

export default BusLines;
