import { useState, useEffect } from 'react';
import '../styles/clock.css';

const Clock = () => {
  const [time, setTime] = useState(new Date());

  console.log(time)

  useEffect(() => {
    const timerId = setInterval(() => setTime(new Date()), 1000);
    return () =>  {
      clearInterval(timerId);
    };
  }, []);

  return (
    <div className="clock">
      <h1>{time.toLocaleDateString()}</h1>
      <h2>{time.toLocaleTimeString()}</h2>
    </div>
  );

}

export default Clock;