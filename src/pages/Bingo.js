import { useState } from 'react';
import '../styles/bingo.css';

const Ball = ({number}) => {
  return (
      <span className="ball">{number}</span>
  )
}

const Bingo = () => {
  const [numbers, setNumbers] = useState([]);

  const getNewNumber = () => {
    const max = 75;
    if (numbers.length < 43) {
      let newNumber = null;
      do {
        newNumber = Math.floor(Math.random()*max + 1);
      } while (numbers.includes(newNumber))
      setNumbers(oldNumbers => [...oldNumbers, newNumber]);
    }
  }

  return (
    <div className="bingo">
      <button onClick={getNewNumber}>+</button>
      <button onClick={()=> setNumbers([])}>reset</button>
      <div className="bingoNumbers">
        {numbers.map(number => <Ball key={number} number={number}/>)}
      </div>
    </div>
  )   
}

export default Bingo;