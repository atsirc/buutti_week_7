import {useState} from 'react';
import '../styles/todo.css';
import {v1 as uuidV1} from 'uuid';

const Form = ({addTodo, input, handleChange}) => {
  return (
    <form onSubmit={(ev) => addTodo(ev)}>
      <input type="text" value={input} onChange={handleChange}/>
      <button type="submit">Submit</button>
    </form>
  );
}

const Todo = ({todos, changeDone, remove}) => {

  return (
    <ul>
      { todos.map( todo => (
      <li key={todo.key}>
        {todo.text}
        <input type="checkbox" onChange={() => changeDone(todo)} checked={todo.done}/>
        <button onClick={() => remove(todo)}>Remove</button>
      </li>
      ))}
    </ul>
  );
}
const Todos = () =>  {
  const [todos, setTodos] = useState([]);
  const [input, setInput] = useState('');

  const addTodo = (ev) => {
    console.log(ev)
    ev.preventDefault();
    const todo = {
      text: input,
      key: uuidV1(),
      done: false
    }

    const newTodos = todos.concat(todo);
    setTodos(newTodos);
    setInput('');
  }

  const onChange = ({target}) => {
    console.log(target);
    setInput(target.value);
  }

  const setDone = (todo) => {
    const newtodos = todos.map(t => {
      if (t.key === todo.key) {
        t.done = !t.done;
      }
      return t;
    });
    setTodos(newtodos);
  }

  const removeTodo = (todo) => {
    setTodos([...todos.filter(t => t.key !== todo.key)]);
  }

  return (
    <div className="todos">
      <Form addTodo={addTodo} input={input} handleChange={onChange}/>
      <h1>Todos</h1>
      <Todo todos={todos} changeDone={setDone} remove={removeTodo}/>
    </div>
  );
}

export default Todos;
