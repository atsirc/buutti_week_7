import '../styles/cards.css';
import {useState, useEffect} from 'react';
import {v1 as uuidV1} from 'uuid';
const colors = ["grey", "orange","pink", "magenta", "blue", "black", "yellow", "green", "lime", "white"];

const createPairs = (size) => {
  const placement = []
  const values = colors.slice(0,size);
  console.log(values);
  while (placement.length !== 2 * size) {
    const num = Math.floor(Math.random()*values.length);
    const color = values[num];
    const previouslyAdded = placement.filter(c => c.color === color);
    if (previouslyAdded.length < 2) {
      const id = uuidV1();
      const vals = { color, id}
      placement.push(vals);
    }
  }
  return placement;
}

const Card = ({card,  isOpen, handleClick}) => {
  const showCard = {
    transform: 'rotateX(180deg)'
  };

  const cardColor = {
    backgroundColor:card.color
  };

return (
  <div className="flip-card" >
    <div className="flip-card-inner" style={isOpen ? showCard : {}} key={'a'+card.id}>
      <div className="flip-card-front" onClick={()=> handleClick(card)}>
      </div>
      <div className="flip-card-back" style={cardColor}>
      </div>
    </div>
  </div>
 )
}

const Cards = ({size=8}) => {
  const [cards, setCards] = useState([]);
  const [history, setHistory] = useState([]);
  const [openCards, setOpenCards] = useState([]);
  const [matchedCards, setMatchedCards] = useState([]);

  useEffect(() => {
    setCards(createPairs(size));
  }, [size])

  useEffect(()=> {
    console.log('handling open cards')
    if (openCards.length === 2) {
      if (openCards[0].color === openCards[1].color) {
        setMatchedCards(prev => [...prev, ...openCards.map(card => card.id)])
      } 
      setTimeout(()=> setOpenCards([]), 500)
    }
  }, [openCards, cards])


  const handleChange = (card) => {
    console.log('handlechange')
    setHistory([...history, card])
    setOpenCards([...openCards, card])
  }

  return (
    <div className="card-game">
      {cards.map(card => {
        return <Card 
                key={card.id} 
                card={card} 
                isOpen={openCards.find(c => c.id === card.id) || matchedCards.includes(card.id)}
                handleClick={handleChange}/>
      })}
    </div>
  )
}

export default Cards;
